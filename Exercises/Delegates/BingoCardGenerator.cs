﻿using System.Collections.Generic;

namespace Exercises.Delegates
{
    public class BingoCardGenerator : IBingoCardsGenerator
    {
        private readonly IEnumerator<IEnumerable<int>> _cards;

        public BingoCardGenerator(IEnumerable<IEnumerable<int>> cards)
        {
            _cards = cards.GetEnumerator();
        }

        public IEnumerable<int> NextBingoCard()
        {
            if (_cards.MoveNext()) return _cards.Current;
            _cards.Reset();
            _cards.MoveNext();
            return _cards.Current;
        }

        public void Reset()
        {
            _cards.Reset();
        }
    }
}