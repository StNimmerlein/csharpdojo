﻿using System.Collections.Generic;

namespace Exercises.Delegates
{
    public interface IPlayer
    {
        public string Name { get; set; }

        public IEnumerable<int> Card { get; set; }
    }
}