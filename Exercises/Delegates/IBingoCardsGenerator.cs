﻿using System.Collections.Generic;

namespace Exercises.Delegates
{
    public interface IBingoCardsGenerator
    {
        IEnumerable<int> NextBingoCard();
        
        void Reset();
    }
}