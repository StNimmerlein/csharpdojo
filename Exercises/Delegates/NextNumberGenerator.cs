﻿using System.Collections.Generic;

namespace Exercises.Delegates
{
    public class NextNumberGenerator : INextNumberGenerator
    {
        private readonly IEnumerator<int> _numberSequence;

        public NextNumberGenerator(IEnumerable<int> numberSequence)
        {
            _numberSequence = numberSequence.GetEnumerator();
        }

        public void Reset()
        {
            _numberSequence.Reset();
        }

        public bool TryNext(out int number)
        {
            number = default;
            if (!_numberSequence.MoveNext()) return false;
            number = _numberSequence.Current;
            return true;
        }
    }
}