﻿namespace Exercises.Delegates
{
    public interface INextNumberGenerator
    {
        void Reset();

        bool TryNext(out int number);
    }
}