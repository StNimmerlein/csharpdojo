﻿using System.Collections.Generic;
using System.Linq;

namespace Exercises.Delegates
{
    public class Bingo
    {
        private readonly INextNumberGenerator _nextNumberGenerator;
        private readonly IBingoCardsGenerator _bingoCardsGenerator;

        private bool _bingoCalled;
        private List<IPlayer> _winners = new List<IPlayer>();

        public Bingo(INextNumberGenerator nextNumberGenerator, IBingoCardsGenerator bingoCardsGenerator)
        {
            _nextNumberGenerator = nextNumberGenerator;
            _bingoCardsGenerator = bingoCardsGenerator;
        }

        public List<IPlayer> Play(IEnumerable<IPlayer> players)
        {
            var playerList = players.ToList();
            foreach (var player in playerList)
            {
                player.Card = _bingoCardsGenerator.NextBingoCard();
            }

            while (!_bingoCalled && _nextNumberGenerator.TryNext(out var number))
            {
            }

            var result = new List<IPlayer>(_winners);
            Reset();
            return result;
        }

        private void Reset()
        {
            _bingoCalled = false;
            _winners = new List<IPlayer>();
            _nextNumberGenerator.Reset();
            _bingoCardsGenerator.Reset();
        }
    }
}