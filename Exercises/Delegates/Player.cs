﻿using System;
using System.Collections.Generic;

namespace Exercises.Delegates
{
    public class Player : IPlayer
    {
        private IEnumerable<int> _card;

        public string Name { get; set; }

        public IEnumerable<int> Card
        {
            get => _card;
            set
            {
                if (_card != null) throw new ArgumentException("Card already set!");
                _card = value;
            }
        }
    }
}