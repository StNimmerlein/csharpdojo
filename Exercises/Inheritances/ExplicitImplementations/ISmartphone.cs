﻿namespace Exercises.Inheritances.ExplicitImplementations
{
    public interface ISmartphone
    {
        public string Start();
    }
}