﻿namespace Exercises.Inheritances.ExplicitImplementations
{
    public interface IDevice
    {
        public string Start();
    }
}