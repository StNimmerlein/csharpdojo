﻿namespace Exercises.Inheritances.Polymorphism
{
    public class Device
    {
        public string Start()
        {
            return "Start device";
        }
    }
}