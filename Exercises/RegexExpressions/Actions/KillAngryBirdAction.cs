﻿namespace Exercises.RegexExpressions.Actions
{
    public class KillAngryBirdAction : IAction
    {
        private readonly AngryBird _angryBird;

        public KillAngryBirdAction(AngryBird angryBird)
        {
            _angryBird = angryBird;
        }

        public override string ToString()
        {
            return $"{nameof(KillAngryBirdAction)} {_angryBird}";
        }
    }
}