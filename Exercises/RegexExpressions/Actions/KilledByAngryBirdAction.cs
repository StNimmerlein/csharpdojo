﻿namespace Exercises.RegexExpressions.Actions
{
    public class KilledByAngryBirdAction : IAction
    {
        public override string ToString()
        {
            return nameof(KilledByAngryBirdAction);
        }
    }
}