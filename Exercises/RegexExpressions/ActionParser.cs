﻿using System;
using Exercises.RegexExpressions.Actions;

namespace Exercises.RegexExpressions
{
    public class ActionParser
    {
        private const string AngryBirdVariableName = "bird";

        // uses named variables in matching for easier retrieval of the interesting value 
        private readonly string _killedByPattern = $@"^KilledBy(?<{AngryBirdVariableName}>\w+)$";

        public IAction Parse(string actionName)
        {
            return actionName switch
            {
                var action when TryMatchAngryBirdName(action, _killedByPattern, out _) => new KilledByAngryBirdAction(),
                _ => new DefaultAction()
            };
        }

        private static bool TryMatchAngryBirdName(string input, string pattern, out AngryBird value)
        {
            value = default;
            return input.TryMatch(pattern, out var match) &&
                   // here one can retrieve the named variable from regex
                   Enum.TryParse(match.Groups[AngryBirdVariableName].Value, out value);
        }
    }
}