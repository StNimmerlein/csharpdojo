﻿namespace Exercises.RegexExpressions
{
    public enum AngryBird
    {
        Stella,
        Chuck,
        Bomb,
        Terence
    }
}