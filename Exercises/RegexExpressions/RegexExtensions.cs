﻿using System.Text.RegularExpressions;

namespace Exercises.RegexExpressions
{
    public static class RegexExtensions
    {
        public static bool TryMatch(this string input, string pattern, out Match match)
        {
            return (match = Regex.Match(input, pattern)).Success;
        }
    }
}