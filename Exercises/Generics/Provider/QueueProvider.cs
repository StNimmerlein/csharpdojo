using System.Collections.Generic;

namespace Exercises.Generics.Provider
{
    public abstract class QueueProvider<T>
    {
        protected Queue<T> ObjectQueue { get; } = new Queue<T>();

        public abstract T GetObject();

        public void AddToQueue(T obj)
        {
            ObjectQueue.Enqueue(obj);
        }
    }
}