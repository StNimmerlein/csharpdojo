namespace Exercises.Generics.Provider
{
    // Use type constraints in order to be able to call new
    public class NextOrNewProvider<T> : QueueProvider<T> 
    {
        public override T GetObject()
        {
            // Use TryDequeue
            throw new System.NotImplementedException();
        }
    }
}