using System.Collections.Generic;

namespace Exercises.Generics
{
    public class TwoLists<TS, TT>
    {
        // FirstCount and SecondCount not implemented, needs connection to lists.
        public int FirstCount => throw new System.NotImplementedException();
        public int SecondCount => throw new System.NotImplementedException();

        private readonly List<TS> _firstList = new List<TS>();
        private readonly List<TT> _secondList = new List<TT>();


        // Add-Methods not implemented. Task is to implement (and make it type safe, so no "object")

        public void Add<T>(T secondListItem)
        {
            // throw new System.NotImplementedException();
        }

        public bool Contains(object element)
        {
            // Initial implementation throws NotImplementedException. Can do with if/else, suggest to look into switch with pattern matching and maybe even switch expression
            throw new System.NotImplementedException();
        }
    }
}