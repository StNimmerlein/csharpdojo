﻿using System.Collections.Generic;

namespace Yield
{
    public class Fibonacci
    {
        public static IEnumerable<int> GetNumbersEnumerable()
        {
            yield return 0;

            var last = 0;
            var current = 1;

            while (true)
            {
                yield return current;
                (current, last) = (current + last, current);
            }
        }
    }
}