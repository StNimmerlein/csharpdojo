using System;
using System.IO;
using System.Linq;
using LiveDemo;
using NUnit.Framework;

namespace LiveDemoTest
{
    [TestFixture]
    public class ScoreCalculatorTest
    {
        // Think about how to implement this step by step
        // Is this a good splitting of the functions?
        // No Func<> but I guess it's better this way. We could think about a separate task for this
        private ScoreCalculator _scoreCalculator;

        [SetUp]
        public void SetUp()
        {
            _scoreCalculator = new ScoreCalculator();
        }

        [Test]
        public void PointParsingWorks()
        {
            Assert.That("3".ExtractValue(), Is.EqualTo(3));
            Assert.That("345".ExtractValue(), Is.EqualTo(345));
            Assert.That("_".ExtractValue(), Is.EqualTo(0));
            Assert.That("/".ExtractValue(), Is.EqualTo(0));
        }

        [Test]
        public void GroupingWorks()
        {
            var lines = File.ReadLines("scores.txt");

            var throwsGroupedByPlayer = _scoreCalculator.GroupScoresByPlayer(lines);

            Assert.That(throwsGroupedByPlayer.Count(), Is.EqualTo(2));
            Assert.That(throwsGroupedByPlayer.Any(groupedThrows => groupedThrows.Key == "Heinz"));
            Assert.That(throwsGroupedByPlayer.Any(groupedThrows => groupedThrows.Key == "Peter"));
        }

        [Test]
        public void GetScoreForPlayerWorksForExistingPlayers()
        {
            var lines = File.ReadLines("scores.txt");
            var throwsGroupedByPlayer = _scoreCalculator.GroupScoresByPlayer(lines);

            Assert.That(_scoreCalculator.GetScoreForPlayer(throwsGroupedByPlayer, "Heinz"), Is.EqualTo(36));
            Assert.That(_scoreCalculator.GetScoreForPlayer(throwsGroupedByPlayer, "Peter"), Is.EqualTo(110));
        }

        [Test]
        public void GetScoreForPlayerWorksForNonExistingPlayers()
        {
            var lines = File.ReadLines("scores.txt");
            var throwsGroupedByPlayer = _scoreCalculator.GroupScoresByPlayer(lines);

            Assert.That(_scoreCalculator.GetScoreForPlayer(throwsGroupedByPlayer, "Andra"), Is.EqualTo(0));
            Assert.That(_scoreCalculator.GetScoreForPlayer(throwsGroupedByPlayer, "Paula"), Is.EqualTo(0));
        }

        [Test]
        public void ToDictionaryWorksAsExpected()
        {
            var lines = File.ReadLines("scores.txt");
            var throwsGroupedByPlayer = _scoreCalculator.GroupScoresByPlayer(lines);
            var scores = _scoreCalculator.ConvertToScoreDictionary(throwsGroupedByPlayer);

            Assert.That(scores.Count(), Is.EqualTo(2));
            Assert.That(scores.ContainsKey("Heinz"), Is.True);
            Assert.That(scores["Heinz"], Is.EqualTo(36));
            Assert.That(scores.ContainsKey("Peter"), Is.True);
            Assert.That(scores["Peter"], Is.EqualTo(110));

            Console.WriteLine(string.Join(Environment.NewLine, scores.Select(score => $"{score.Key}: {score.Value}")));
        }
    }
}