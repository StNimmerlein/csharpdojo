using Delegates;
using NUnit.Framework;

namespace DelegatesTest
{
    public class PrinterTests
    {
        [Test]
        public void Test_EventWasHandled()
        {
            const int expectedPrinterNumber = 5;
            var printerNumber = 0;

            void PrinterOnPrinterFinished(int number)
            {
                printerNumber = number;
            }

            var printer = new Printer(expectedPrinterNumber);
            printer.PrinterFinished += PrinterOnPrinterFinished;

            Assert.That(printerNumber, Is.EqualTo(0));

            printer.Print("Text");

            Assert.That(printerNumber, Is.EqualTo(expectedPrinterNumber));
        }


        [Test]
        public void Test_EventWasNotHandledAfterRemove()
        {
            const int expectedPrinterNumber = 5;
            const int printerNumber = 10;
            var finishedPrinterNumber = 0;

            void PrinterOnPrinterFinished(int number)
            {
                finishedPrinterNumber = number;
            }

            var printer = new Printer(printerNumber);
            printer.PrinterFinished += PrinterOnPrinterFinished;

            printer.Print("Text");
            Assert.That(finishedPrinterNumber, Is.EqualTo(printerNumber));

            finishedPrinterNumber = expectedPrinterNumber;
            printer.PrinterFinished -= PrinterOnPrinterFinished;

            printer.Print("Text");

            Assert.That(finishedPrinterNumber, Is.EqualTo(expectedPrinterNumber));
        }

        [Test]
        public void Test_EventWasOnlyInterceptedByItsListeners()
        {
            const int expectedFirstPrinterNumber = 5;
            const int expectedSecondPrinterNumber = 10;
            var firstPrinterNumber = 0;
            var secondPrinterNumber = 0;

            var firstPrinter = new Printer(expectedFirstPrinterNumber);
            firstPrinter.PrinterFinished += number => firstPrinterNumber = number;
            var secondPrinter = new Printer(expectedSecondPrinterNumber);
            secondPrinter.PrinterFinished += number => secondPrinterNumber = number;
            Assert.That(firstPrinterNumber, Is.EqualTo(0));
            Assert.That(secondPrinterNumber, Is.EqualTo(0));

            firstPrinter.Print("Text");
            secondPrinter.Print("Text");
            Assert.That(firstPrinterNumber, Is.EqualTo(expectedFirstPrinterNumber));
            Assert.That(secondPrinterNumber, Is.EqualTo(expectedSecondPrinterNumber));
        }

        [Test]
        public void Test_EventWasInterceptedByAllItsListeners()
        {
            const int expectedPrinterNumber = 5;
            var printerNumber = 0;
            var printerName = string.Empty;

            var printer = new Printer(expectedPrinterNumber);
            printer.PrinterFinished += number => printerName = $"Printer{number}";
            printer.PrinterFinished += number => printerNumber = number;

            Assert.That(printerNumber, Is.EqualTo(0));
            Assert.That(printerName, Is.Empty);

            printer.Print("Text");

            Assert.That(printerNumber, Is.EqualTo(expectedPrinterNumber));
            Assert.That(printerName, Is.EqualTo($"Printer{expectedPrinterNumber}"));
        }
    }
}