﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace YieldMultipleEvaluations
{
    internal class Program
    {
        private static void Main()
        {
            var initialList = new List<int> {1, 2, 3};
            var enumerable = initialList.Select(e => e);
            var list = initialList.Select(e => e).ToList();

            Console.WriteLine($"Enumerable: {string.Join(',', enumerable)}");
            Console.WriteLine($"List: {string.Join(',', list)}");
            initialList.Add(4);
            Console.WriteLine($"Enumerable: {string.Join(',', enumerable)}");
            Console.WriteLine($"List: {string.Join(',', list)}");
        }
    }
}