namespace LiveDemo
{
    public static class ValueExtractionExtension
    {
        // When implementing, first do with Parse and try/catch, then with TryParse and if, then in one line, than in expression body
        public static int ExtractValue(this string stringValue)
        {
            return int.TryParse(stringValue, out var value) ? value : 0;
        }
    }
}