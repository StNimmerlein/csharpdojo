using System.Collections.Generic;
using System.Linq;

namespace LiveDemo
{
    public class ScoreCalculator
    {
        public IEnumerable<IGrouping<string, Throw>> GroupScoresByPlayer(IEnumerable<string> scoreLines)
        {
            return scoreLines
                .Select(line => line.Split(' '))
                .Select(splitLine => new Throw
                {
                    Name = splitLine[0],
                    Throw1 = splitLine[1].ExtractValue(),
                    Throw2 = splitLine[2].ExtractValue(),
                    Throw3 = splitLine[3].ExtractValue()
                })
                .GroupBy(thrw => thrw.Name);
        }

        public int GetScoreForPlayer(IEnumerable<IGrouping<string, Throw>> groupedThrows, string playerName)
        {
            return groupedThrows
                       .FirstOrDefault(groupedScore => groupedScore.Key == playerName)
                       ?.SelectMany(thrw => new List<int>{thrw.Throw1, thrw.Throw2, thrw.Throw3})
                       .Sum()
                   ?? 0;
        }

        public Dictionary<string, int> ConvertToScoreDictionary(IEnumerable<IGrouping<string, Throw>> groupedThrows)
        {
            return groupedThrows
                .Select(throwsByName => new
                {
                    Name = throwsByName.Key,
                    Score = throwsByName.SelectMany(thr => new List<int> {thr.Throw1, thr.Throw2, thr.Throw3}).Sum()
                })
                .ToDictionary(scoresByName => scoresByName.Name, scoresByName => scoresByName.Score);
        }
    }
}