﻿using System.Collections.Generic;
using System.Linq;
using Exercises.Yield;
using NUnit.Framework;

namespace ExercisesTest.Yield
{
    public class SubsetGeneratorTests
    {
        [Test]
        public void Test_SubsetsCount()
        {
            var values = Enumerable.Range(1, 20);
            Assert.That(values.Subsets().Count(), Is.EqualTo(1048576));
        }

        [Test]
        public void Test_Subsets()
        {
            var ints = new List<int> {1, 2, 3, 4};
            var actual = ints.Subsets().ToList();

            Assert.AreEqual(16, actual.Count);

            var expectedSubsets = new List<IEnumerable<int>>
            {
                new List<int>(),
                new List<int> {1},
                new List<int> {1, 2},
                new List<int> {1, 3},
                new List<int> {1, 2, 3},
                new List<int> {1, 2, 3, 4},
                new List<int> {2},
                new List<int> {2, 3},
                new List<int> {2, 4},
                new List<int> {2, 3, 4},
                new List<int> {3},
                new List<int> {3, 4},
                new List<int> {4}
            };
            foreach (var expectedSubset in expectedSubsets)
                Assert.IsTrue(actual.Exists(subset => subset.SequenceEqual(expectedSubset)));
        }

        [Test]
        public void Subsets_IEnumerable()
        {
            var ints = new List<int>();
            var actual = ints.Subsets();
            ints.Add(1);

            Assert.AreEqual(2, actual.Count());
        }

        [TestCase(new[] {-1}, ExpectedResult = 2)]
        [TestCase(new[] {-2, -1}, ExpectedResult = 5)]
        [TestCase(new[] {-3, -2, -1}, ExpectedResult = 16)]
        public int Test_SubsetsWithSum(IEnumerable<int> negativeValues)
        {
            var values = negativeValues.Union(Enumerable.Range(1, 20));
            return values.SubsetsWithSum(0).Count();
        }

        [Test]
        public void SubsetsWithSum_IEnumerable()
        {
            var ints = new List<int> {1};
            var actual = ints.SubsetsWithSum(3);
            ints.Add(2);

            Assert.AreEqual(1, actual.Count());
            CollectionAssert.AreEquivalent(new List<int> {1, 2}, actual.First());
        }
    }
}