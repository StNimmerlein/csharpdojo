﻿using System.Collections.Generic;
using System.Linq;
using Exercises.Delegates;
using NUnit.Framework;

namespace ExercisesTest.Delegates
{
    public class BingoIntegrationTests
    {
        private NextNumberGenerator _numberGenerator;

        [SetUp]
        public void Setup()
        {
            _numberGenerator = new NextNumberGenerator(new List<int> {1, 2, 3, 4, 5});
        }

        [Test]
        public void Test_NoWinner()
        {
            var bingo = new Bingo(_numberGenerator, new BingoCardGenerator(new List<IEnumerable<int>>
            {
                new List<int> {50, 60, 70}
            }));

            var winner = bingo.Play(new List<Player>()
            {
                new Player() {Name = "Unknown"}
            });

            Assert.That(winner, Is.Empty);
        }

        [Test]
        public void Test_StopAfterBingoWasCalled()
        {
            const string expectedWinnerName = "Marco";
            var cardGenerator = new BingoCardGenerator(new List<IEnumerable<int>>
            {
                new List<int> {1, 2, 3, 5},
                new List<int> {1, 2, 3, 4},
            });
            var players = new List<Player> {new Player {Name = "Sieben"}, new Player {Name = expectedWinnerName}};
            var bingo = new Bingo(_numberGenerator, cardGenerator);

            var winners = bingo.Play(players);

            Assert.That(winners.Count, Is.EqualTo(1));
            Assert.That(winners.First().Name, Is.EqualTo(expectedWinnerName));
        }


        [Test]
        public void Test_MultipleWinners()
        {
            const string firstWinnerName = "Marco";
            const string secondWinnerName = "Sieben";
            var players = new List<Player>
            {
                new Player {Name = firstWinnerName}, new Player {Name = "Unknown"}, new Player {Name = secondWinnerName}
            };
            var expectedWinnerNames = new List<string> {firstWinnerName, secondWinnerName};
            var cardGenerator = new BingoCardGenerator(new List<IEnumerable<int>>
            {
                new List<int> {1, 2, 3, 4},
                new List<int> {1, 2, 3, 5},
            });
            var bingo = new Bingo(_numberGenerator, cardGenerator);

            var winnerNames = bingo.Play(players).Select(winner => winner.Name);

            CollectionAssert.AreEquivalent(expectedWinnerNames, winnerNames);
        }

        [Test]
        public void Test_ResetGameStateBeforePlay()
        {
            const string expectedWinner = "Winner";
            var bingo = new Bingo(_numberGenerator, new BingoCardGenerator(new List<IEnumerable<int>>
            {
                new List<int>
                {
                    1, 2, 3
                }
            }));

            bingo.Play(new List<Player> {new Player() {Name = "Unknown"}});
            var winners = bingo.Play(new List<Player> {new Player() {Name = expectedWinner}});

            Assert.That(winners.Count, Is.EqualTo(1));
            Assert.That(winners.First().Name, Is.EqualTo(expectedWinner));
        }

        [Test]
        public void Test_ResetEverythingBeforePlay()
        {
            const string expectedWinner = "Winner";
            var bingo = new Bingo(_numberGenerator, new BingoCardGenerator(new List<IEnumerable<int>>
            {
                new List<int>
                {
                    1, 2, 3
                },
                new List<int> {50, 3, 5}
            }));

            bingo.Play(new List<Player> {new Player() {Name = "Unknown"}});
            var winners = bingo.Play(new List<Player> {new Player() {Name = expectedWinner}});

            Assert.That(winners.Count, Is.EqualTo(1));
        }
    }
}