﻿using Exercises.Inheritances.Polymorphism;
using NUnit.Framework;

namespace ExercisesTest.Inheritances.Polymorphism
{
    public class InheritancesTests
    {
        [Test]
        public void Polymorphism_NoOverride()
        {
            Mobile mobile = new Mobile();
            Device mobileDevice = new Mobile();
            
            Assert.That(mobile.Start(), Is.EqualTo("Start mobile"));
            Assert.That(mobileDevice.Start(), Is.EqualTo("Start device"));
        }
        
        [Test]
        public void Polymorphism_Override()
        {
            Tablet tablet = new Tablet();
            Device tabletDevice = new Tablet();
            
            Assert.That(tablet.Start(), Is.EqualTo("Start tablet"));
            Assert.That(tabletDevice.Start(), Is.EqualTo("Start tablet"));
        }
    }
}