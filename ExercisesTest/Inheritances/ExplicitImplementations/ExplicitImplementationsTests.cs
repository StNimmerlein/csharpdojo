﻿using Exercises.Inheritances.ExplicitImplementations;
using NUnit.Framework;

namespace ExercisesTest.Inheritances.ExplicitImplementations
{
    public class ExplicitImplementationsTests
    {
        [Test]
        public void MultipleInterfaces_SameImplementation()
        {
            Iphone iphone = new Iphone();
            IDevice device = new Iphone();
            ISmartphone smartphone = new Iphone();
            
            Assert.That(iphone.Start(), Is.EqualTo("Start iphone"));
            Assert.That(device.Start(), Is.EqualTo("Start iphone"));
            Assert.That(smartphone.Start(), Is.EqualTo("Start iphone"));
        }
        
        [Test]
        public void MultipleInterfaces_MultipleImplementations()
        {
            Huawei huawei = new Huawei();
            IDevice device = new Huawei();
            ISmartphone smartphone = new Huawei();
            
            Assert.That(huawei.Start(), Is.EqualTo("Start Huawei"));
            Assert.That(device.Start(), Is.EqualTo("Start device"));
            Assert.That(smartphone.Start(), Is.EqualTo("Start smartphone"));
        }
    }
}