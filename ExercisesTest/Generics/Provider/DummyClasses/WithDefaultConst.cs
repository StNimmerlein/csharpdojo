namespace ExercisesTest.Generics.Provider.DummyClasses
{
    public class WithDefaultConst
    {
        public static readonly string DefaultName = "defaultConstructorName";

        public string Name { get; set; }

        public WithDefaultConst()
        {
            Name = DefaultName;
        }
    }
}