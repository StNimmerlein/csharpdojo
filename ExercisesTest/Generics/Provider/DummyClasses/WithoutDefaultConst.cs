namespace ExercisesTest.Generics.Provider.DummyClasses
{
    public class WithoutDefaultConst
    {
        public string Name { get; set; }

        public WithoutDefaultConst(string name)
        {
            Name = name;
        }
    }
}