using Exercises.Generics.Provider;
using ExercisesTest.Generics.Provider.DummyClasses;
using NUnit.Framework;

namespace ExercisesTest.Generics.Provider
{
    [TestFixture]
    public class NextOrDefaultQueueProviderTest
    {
        [Test]
        public void NextOrDefaultProviderProvidesObjectsFromQueue()
        {
            var provider = new NextOrDefaultProvider<WithDefaultConst>();

            provider.AddToQueue(new WithDefaultConst {Name = "first"});
            provider.AddToQueue(new WithDefaultConst {Name = "second"});

            Assert.That(provider.GetObject().Name, Is.EqualTo("first"));
            provider.AddToQueue(new WithDefaultConst {Name = "surprise"});
            Assert.That(provider.GetObject().Name, Is.EqualTo("second"));
            Assert.That(provider.GetObject().Name, Is.EqualTo("surprise"));
        }

        [Test]
        public void NextOrDefaultProviderProvidesNullForObjectsWhenQueueEmpty()
        {
            var provider = new NextOrDefaultProvider<WithoutDefaultConst>();

            Assert.That(provider.GetObject(), Is.Null);
            provider.AddToQueue(new WithoutDefaultConst("custom"));
            Assert.That(provider.GetObject().Name, Is.EqualTo("custom"));
            Assert.That(provider.GetObject(), Is.Null);
        }

        [Test]
        public void NextOrDefaultProviderReturnsDefaultForPrimitives()
        {
            Assert.That(new NextOrDefaultProvider<int>().GetObject(), Is.EqualTo(0));
            Assert.That(new NextOrDefaultProvider<decimal>().GetObject(), Is.EqualTo(0));
            Assert.That(new NextOrDefaultProvider<bool>().GetObject(), Is.EqualTo(false));
            // Even though string is primitive, it is still a reference type and thus has a default of null
            Assert.That(new NextOrDefaultProvider<string>().GetObject(), Is.EqualTo(null));
        }
    }

    [TestFixture]
    public class NextOrNewQueueProviderTest
    {
        [Test]
        public void NextOrNewProviderNeedsDefaultConstructor()
        {
            // Should not compile
            var provider = new NextOrNewProvider<WithoutDefaultConst>();
            Assert.Fail();
        }

        [Test]
        public void NextOrNewProviderProvidesObjectsFromQueue()
        {
            var provider = new NextOrNewProvider<WithDefaultConst>();

            provider.AddToQueue(new WithDefaultConst {Name = "first"});
            provider.AddToQueue(new WithDefaultConst {Name = "second"});

            Assert.That(provider.GetObject().Name, Is.EqualTo("first"));
            provider.AddToQueue(new WithDefaultConst {Name = "surprise"});
            Assert.That(provider.GetObject().Name, Is.EqualTo("second"));
            Assert.That(provider.GetObject().Name, Is.EqualTo("surprise"));
        }

        [Test]
        public void NextOrNewProviderProvides_New_InstanceWhenQueueEmpty()
        {
            var provider = new NextOrNewProvider<WithDefaultConst>();

            var firstObject = provider.GetObject();
            Assert.That(firstObject.Name, Is.EqualTo(WithDefaultConst.DefaultName));

            provider.AddToQueue(new WithDefaultConst {Name = "custom"});
            Assert.That(provider.GetObject().Name, Is.EqualTo("custom"));
            var lastObject = provider.GetObject();
            Assert.That(lastObject.Name, Is.EqualTo(WithDefaultConst.DefaultName));
            Assert.That(firstObject, Is.Not.EqualTo(lastObject));
        }
    }
}