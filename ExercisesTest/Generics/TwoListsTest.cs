using Exercises.Generics;
using GenericsTest.DummyClasses;
using NUnit.Framework;

namespace ExercisesTest.Generics
{
    [TestFixture]
    public class TwoListsTest
    {
        [Test]
        public void TwoListsDecidesCorrectlyOnWhichListToAdd()
        {
            var twoLists = new TwoLists<int, string>();

            twoLists.Add(3);
            twoLists.Add(5);
            twoLists.Add("Ernst");
            twoLists.Add(7);

            Assert.That(twoLists.FirstCount, Is.EqualTo(3));
            Assert.That(twoLists.SecondCount, Is.EqualTo(1));
        }

        [Test]
        public void TwoListsDoesNotWorkWithSameTypes()
        {
            var twoLists = new TwoLists<int, int>();

            // Does not compile
            // twoLists.Add(3);
            // But conclusive calls work, see Assertions in this test

            Assert.That(twoLists.FirstCount, Is.EqualTo(0));
            Assert.That(twoLists.SecondCount, Is.EqualTo(0));
        }

        [Test]
        public void TwoListsCanDifferentiateBetweenDifferentTypes()
        {
            var twoLists = new TwoLists<A, C>();

            twoLists.Add(new A());
            twoLists.Add(new C());

            Assert.That(twoLists.FirstCount, Is.EqualTo(1));
            Assert.That(twoLists.SecondCount, Is.EqualTo(1));
        }

        [Test]
        public void TwoListsWorksWithInheritance()
        {
            var twoListsDistinct = new TwoLists<A, C>();
            var twoListsInherited = new TwoLists<A, B>();

            twoListsDistinct.Add(new A());
            twoListsDistinct.Add(new B());
            twoListsInherited.Add(new A());
            twoListsInherited.Add(new B());

            Assert.That(twoListsDistinct.FirstCount, Is.EqualTo(2));
            Assert.That(twoListsDistinct.SecondCount, Is.EqualTo(0));
            Assert.That(twoListsInherited.FirstCount, Is.EqualTo(1));
            Assert.That(twoListsInherited.SecondCount, Is.EqualTo(1));
        }

        [Test]
        public void CanDecideIfContained()
        {
            var twoLists = new TwoLists<int, string>();

            twoLists.Add(1);
            twoLists.Add(2);
            twoLists.Add("3");
            twoLists.Add("3.14");
            twoLists.Add("Hugo");

            Assert.That(twoLists.Contains(0), Is.False);
            Assert.That(twoLists.Contains(1), Is.True);
            Assert.That(twoLists.Contains(3), Is.False);
            Assert.That(twoLists.Contains("3"), Is.True);
            Assert.That(twoLists.Contains(3.14), Is.False);
            Assert.That(twoLists.Contains("3.14"), Is.True);
            Assert.That(twoLists.Contains("Hugo"), Is.True);
            Assert.That(twoLists.Contains(new A()), Is.False);
        }

        // Bonus investigations (not related to generics but more to int, double, float, decimal).
        // Bonus 1: When you use new TwoLists<int, float>(), how do you decide whether to check for an item in the int or in the float list. Can you check for a double? And the other way round?
        // Bonus 2: When you use TwoLists<float, double> and you invoke Add(3), is this an error because 3 is neither float nor double, or is it implicitly casted? If so, to float or double?
        // Bonus 3: How is the behavior in Bonus 1 and 2 if you use "decimal" in place of one of the number types?
    }
}