using System;
using Exercises.RegexExpressions;
using Exercises.RegexExpressions.Actions;
using NUnit.Framework;

namespace ExercisesTest.RegexExpressions
{
    public class ActionParserTest
    {
        private readonly ActionParser _uut = new ActionParser();

        [Test]
        public void Parse_ReturnDefaultActionWhenUnknown()
        {
            Assert.IsInstanceOf<DefaultAction>(_uut.Parse("unknownAction"));
        }

        [Test]
        public void Parse_KilledBy([Values] AngryBird angryBird)
        {
            var action = _uut.Parse($"KilledBy{angryBird}");
            Console.WriteLine(action);
            Assert.IsInstanceOf<KilledByAngryBirdAction>(action);
        }

        [Test]
        public void Parse_Not_KilledBy_WhenAngryBirdUnknown()
        {
            var action = _uut.Parse("KilledByUnknownAngryBird_12345");
            Assert.IsNotInstanceOf<KilledByAngryBirdAction>(action);
            Assert.IsInstanceOf<DefaultAction>(action);
        }

        [Test]
        public void Parse_KillAngryBird([Values] AngryBird angryBird)
        {
            var actual = _uut.Parse($"Kill{angryBird}");
            var actionString = actual.ToString();
            
            Assert.IsInstanceOf<KillAngryBirdAction>(actual);
            // TODO:  what happens if we rename the class?
            Assert.That(actionString, Contains.Substring(nameof(KillAngryBirdAction)));
            Assert.That(actionString, Contains.Substring(angryBird.ToString()));
        }

        [Test]
        public void Parse_Not_KillAngryBird_WhenAngryBirdUnknown()
        {
            var action = _uut.Parse("KillImaginaryUnknownAngryBird_1234");
            Assert.IsNotInstanceOf<KillAngryBirdAction>(action);
            Assert.IsInstanceOf<DefaultAction>(action);
        }
    }
}