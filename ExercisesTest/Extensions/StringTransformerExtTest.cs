﻿using NUnit.Framework;

namespace ExercisesTest.Extensions
{
    [TestFixture]
    public class StringTransformerExtTest
    {
        /* TODO: Make tests pass:
            - uncomment the test instructions
            - remove Assert.Fail()
            - implement the extension methods in StringTransfomerExt
         */

        // Suggest to use ?., SelectMany, ?? and do everything in one line/expression
        [Test]
        public void EmptyIfNullWorks()
        {
            // Assert.That("ich bin nicht null".EmptyIfNull(), Is.EqualTo("ich bin nicht null"));
            // Assert.That("".EmptyIfNull(), Is.EqualTo(""));
            // Assert.That(((string) null).EmptyIfNull(), Is.Empty);
            Assert.Fail();
        }

        [Test]
        public void ToSnakeCaseWorksAsExpected()
        {
            // Assert.That("ichbinklein".ToSnakeCase(), Is.EqualTo("ichbinklein"));
            // Assert.That("ichBinEinKamel".ToSnakeCase(), Is.EqualTo("ich_bin_ein_kamel"));
            Assert.Fail();
        }

        [Test]
        public void ToSnakeCaseOfNullIsEmptyString()
        {
            // Assert.That(((string) null).ToSnakeCase(), Is.Empty);
            Assert.Fail();
        }
    }
}