﻿using NUnit.Framework;

namespace ExercisesTest.Properties
{
    [TestFixture]
    public class PersonTest
    {
        // TODO: Step by step uncomment the tests and implement the properties
        
        [Test]
        public void SettingNamesSeparatelyWorks()
        {
            // var person = new Person();
            //
            // person.FirstName = "Hans";
            // person.LastName = "Meier";
            //
            // Assert.That(person.FirstName, Is.EqualTo("Hans"));
            // Assert.That(person.LastName, Is.EqualTo("Meier"));
            Assert.Fail();
        }

        [Test]
        public void LastNameKnownWorks()
        {
            // Assert.That(new Person().LastNameKnown, Is.False);
            // Assert.That(new Person {FirstName = "Hans"}.LastNameKnown, Is.False);
            // Assert.That(new Person {LastName = "Meier"}.LastNameKnown, Is.True);
            // Assert.That(new Person {LastName = ""}.LastNameKnown, Is.True);
            Assert.Fail();
        }

        [Test]
        public void GettingCombinedNameWorks()
        {
            // var person = new Person
            // {
            //     FirstName = "Hans",
            //     LastName = "Meier"
            // };
            //
            //
            // Assert.That(person.Name, Is.EqualTo("Hans Meier"));
            Assert.Fail();
        }

        [Test]
        public void CombinedNameTrims()
        {
            // Assert.That(new Person {FirstName = "Hans"}.Name, Is.EqualTo("Hans"));
            // Assert.That(new Person {LastName = "Meier"}.Name, Is.EqualTo("Meier"));
            // Assert.That(new Person {FirstName = "  Hans  ", LastName = " Meier "}.Name, Is.EqualTo("Hans    Meier"));
            // Assert.That(new Person().Name, Is.Empty);
            Assert.Fail();
        }

        [Test]
        public void SettingCombinedNameWorks()
        {
            // var person = new Person();
            //
            // person.Name = "Hans Meier";
            //
            // Assert.That(person.Name, Is.EqualTo("Hans Meier"));
            // Assert.That(person.FirstName, Is.EqualTo("Hans"));
            // Assert.That(person.LastName, Is.EqualTo("Meier"));
            // Assert.That(person.LastNameKnown, Is.True);
            Assert.Fail();
        }

        [Test]
        public void SettingCombinedNameOnlyUsesFirstAndLast()
        {
            // var person = new Person();
            //
            // person.Name = "Hans Joachim Benedikt Meier";
            //
            // Assert.That(person.Name, Is.EqualTo("Hans Meier"));
            // Assert.That(person.FirstName, Is.EqualTo("Hans"));
            // Assert.That(person.LastName, Is.EqualTo("Meier"));
            Assert.Fail();
        }

        [Test]
        public void SettingCombinedSingleNameOnlySetsFirstName()
        {
            // var person = new Person();
            // person.Name = "Hans";
            //
            // Assert.That(person.Name, Is.EqualTo("Hans"));
            // Assert.That(person.FirstName, Is.EqualTo("Hans"));
            // Assert.That(person.LastName, Is.Null);
            // Assert.That(person.LastNameKnown, Is.False);
            Assert.Fail();
        }

        [Test]
        public void SettingNullAsNameDoesNotFail()
        {
            // var person = new Person {Name = "Hans Meier"};
            // person.Name = null;
            //
            // Assert.That(person.FirstName, Is.Null);
            // Assert.That(person.LastName, Is.Null);
            // Assert.That(person.Name, Is.Empty);
            Assert.Fail();
        }

        [Test]
        public void AliveCanBeSet()
        {
            // var person = new Person();
            //
            // person.Alive = false;
            //
            // Assert.IsFalse(person.Alive);
            Assert.Fail();
        }

        [Test]
        public void AliveIsTrueByDefault()
        {
            // var person = new Person();
            //
            // Assert.IsTrue(person.Alive);
            Assert.Fail();
        }

        [Test]
        public void LastNameChanges_KeepsTrackOfLastNameChanges()
        {
            // var person = new Person();
            //
            // Assert.That(person.LastNameChanges, Is.EqualTo(0));
            //
            // person.LastName = "Meier";
            // Assert.That(person.LastNameChanges, Is.EqualTo(1));
            // // This counting is dumb; it only keeps track of the setter, not the actual name
            // person.LastName = "Meier";
            // Assert.That(person.LastNameChanges, Is.EqualTo(2));
            //
            // person.FirstName = "Hans";
            // Assert.That(person.LastNameChanges, Is.EqualTo(2));
            //
            // person.Name = "Hugo";
            // Assert.That(person.LastNameChanges, Is.EqualTo(3));
            //
            // person.Name = "Ernst Huber";
            // Assert.That(person.LastNameChanges, Is.EqualTo(4));
            Assert.Fail();
        }

        [Test]
        public void LastNameChanges_CannotBeSetFromTheOutside()
        {
            // var person = new Person();

            // This should not compile   
            // person.LastNameChanges = 2;
        }

        // TODO Bonus exercise:
        // What is the value of LastNameChanges when you do new Person {LastName = "Meier"}?
        // Can you think of a way to initialize a person with a last name and have the LastNameChanges still at 0
        // (obviously you can; implement it)
    }
}