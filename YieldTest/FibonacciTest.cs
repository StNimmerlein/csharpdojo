using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Yield;

namespace YieldTest
{
    [TestFixture]
    public class FibonacciTest
    {
        [Test]
        public void EnumerableContainsFibonacciNumbers()
        {
            var numbers = Fibonacci.GetNumbersEnumerable();
            Assert.That(numbers.Take(7), Is.EquivalentTo(new List<int> {0, 1, 1, 2, 3, 5, 8}));
        }

        [Test]
        public void EnumerableCanBeSkipped()
        {
            var numbers = Fibonacci.GetNumbersEnumerable();
            Assert.That(numbers.Skip(7).Take(3), Is.EquivalentTo(new List<int> {13, 21, 34}));
        }

        [Test]
        public void EnumerableCanGoUpAsYouLike()
        {
            var numbers = Fibonacci.GetNumbersEnumerable();

            // Play around and try different limits. No matter how high you go, the requested number will always be "contained" in the enumerable
            Assert.That(numbers.First(number => number > 1000), Is.EqualTo(1597));
        }

        [Test]
        public void EnumerableIsEndless()
        {
            var numbers = Fibonacci.GetNumbersEnumerable();

            // This will never terminate
            // numbers.Average();
        }
    }
}