﻿using System;

namespace Delegates
{
    public class Printer
    {
        private readonly int _number;

        public Printer(int number)
        {
            _number = number;
        }

        public delegate void PrinterFinishedHandler(int printerNumber);

        public event PrinterFinishedHandler PrinterFinished;

        public void Print(string input)
        {
            // print or do whatever you do 
            PrinterFinished?.Invoke(_number);
        }
    }
}